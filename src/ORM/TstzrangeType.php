<?php

namespace Fuzzy\Project\ORM;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class TstzrangeType extends Type {

    const TSTZRANGE = 'tstzrange';

    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform) {
        return $platform->getDoctrineTypeMapping('tstzrange');
    }

    public function convertToPHPValue($value, AbstractPlatform $platform) {
        $value = str_replace(array('[',']'),'',$value);
        $parts = explode(',',$value);
        return [new \DateTime($parts[0]),new \DateTime($parts[1])];
    }

    //(longitude, latitude)
    public function convertToDatabaseValue($value, AbstractPlatform $platform) {
        return '[' . $value[0]->format('Y-m-d H:i'). ',' .$value[1]->format('Y-m-d H:i') . ']';
    }

    public function getName() {
        return self::TSTZRANGE;
    }
}
