<?php

    namespace DevXYZ\Project\Entity;

    /**
     * @Entity(repositoryClass="DevXYZ\Project\Repository\Tag")
     * @Table(name="tags")
     */
    class Tag extends Base {

        /**
         * @Column(type="string", length=255, nullable=false)
         */
        public $tag;
    }

