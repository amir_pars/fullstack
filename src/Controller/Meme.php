<?php

namespace DevXYZ\Project\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class Meme extends Base {

    protected $repository;

    function __construct() {
        parent::__construct();
        $this->repository = $this->em->getRepository('DevXYZ\Project\Entity\Meme');
    }

    /**
     * @api {post} /meme meme creates a meme
     * @apiName create
     * @apiGroup Meme
     */
    public function create(Request $request, $input) {
        $meme = new \DevXYZ\Project\Entity\Meme();

        if(!strlen($this->payload['title'])) {
            $response = new JsonResponse('missing title');
            $response->setStatusCode(Response::HTTP_BAD_REQUEST);
            return $response;
        }

        //todo: xss cleaning
        $meme->title = strip_tags($this->payload['title']);

        if(!empty($this->payload['tags'])) {

            $tags = explode(',',$this->payload['tags']);
            $tagRepo = $this->em->getRepository('DevXYZ\Project\Entity\Tag');

            foreach($tags as $tag) {
                $tagEntity = $tagRepo->findOneBy(['tag' => $tag]);

                if(!$tagEntity) {
                    $tagEntity = new \DevXYZ\Project\Entity\Tag();
                    $tagEntity->tag = $tag;
                    $this->em->persist($tagEntity);
                    $this->em->flush();
                }

                $meme->tags->add($tagEntity);
            }
        }

        $this->em->persist($meme);
        $this->em->flush();
        return new JsonResponse($meme->id);
    }

    /**
     * @api {post} /memes
     * @apiName retrieve all
     * @apiGroup Meme
     */
    public function retrieve_all(Request $request, $input) {

        $sql = "select m.id, m.title, t.tag, t.id as tag_id
            from memes m
            left join memes_tags_join mtj on mtj.meme_id = m.id
            left join tags t on t.id = mtj.tag_id
            where true ";

        if($this->payload['title']) {
            $sql .= " and lower(m.title) like :title ";
        }

        if($this->payload['tags']) {
            $tags = explode(',',$this->payload['tags']);
            foreach($tags as $k => $tag) {
                $sql .= " and exists (select 1 from memes_tags_join mtj2
                    join tags t2 on t2.id = mtj2.tag_id
                    where mtj2.meme_id = m.id
                    and lower(tag) = :tag$k) ";
            }
        }

        file_put_contents('/tmp/t',$sql);

        $connection = $this->em->getConnection();
        $statement = $connection->prepare($sql);

        if($this->payload['title']) {
            $statement->bindValue('title', strtolower($this->payload['title']));
        }

        if(isset($tags)) {
            foreach($tags as $k => $tag) {
                $statement->bindValue("tag$k", strtolower($tag));
            }
        }

        $statement->execute();

        $memes = $statement->fetchAll();
        $result = array();

        foreach($memes as $meme) {
            if(!isset($result[$meme['id']])) {
               $result[$meme['id']] = [
                   'id' => $meme['id'],
                   'title' => $meme['title'],
                   'tags' => ['id' => $meme['tag_id'], 'tag' => $meme['tag']]
               ];
            }
            else {
                $result[$meme['id']]['tags'][] = ['id' => $meme['tag_id'], 'tag' => $meme['tag']];
            }
        }

        return new JsonResponse(array_values($result));
    }

    /**
     * @api {get} /meme/{id} meme
     * @apiName report
     * @apiGroup Meme
     */
    public function retrieve(Request $request, $input) {
        $meme = $this->repository->find($input['id']);

        if(!$meme) {
            $response = new JsonResponse('meme not found');
            $response->setStatusCode(Response::HTTP_NOT_FOUND);
            return $response;
        }

        return new JsonResponse([
            'id' => $meme->id,
            'title' => $meme->title,
            'tags' => $meme->tags->toArray()
        ]);
    }

    /**
     * @api {put} /meme/{id} meme updates a meme
     * @apiName report
     * @apiGroup Meme
     */
    public function update(Request $request, $input) {
        return new JsonResponse('yah');
    }

    /**
     * @api {delete} /meme/{id} meme deletes a meme
     * @apiName report
     * @apiGroup Meme
     */
    public function delete(Request $request, $input) {
        $meme = $this->repository->find($input['id']);

        if(!$meme) {
            $response = new JsonResponse('meme not found');
            $response->setStatusCode(Response::HTTP_NOT_FOUND);
            return $response;
        }

        $this->em->remove($meme);
        $this->em->flush();

        return new JsonResponse('meme deleted successfully');
    }


}