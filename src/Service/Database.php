<?php

namespace DevXYZ\Project\Service;
use Doctrine\ORM\Tools\Setup;
use Symfony\Component\Yaml\Yaml;
use Doctrine\DBAL\Types\Type;

class Database {

    public static function getEntityManager() {
        $settings = Yaml::parse(file_get_contents(__DIR__ . "/../../config/config.yml"));

        $isDevMode = true;
        $config = Setup::createAnnotationMetadataConfiguration(array(__DIR__."/../Entity"), $isDevMode);

        Type::addType('tsrange', 'DevXYZ\Project\ORM\TsrangeType');

        // obtaining the entity manager
        $em = \Doctrine\ORM\EntityManager::create($settings['database'], $config);
        $em->getConnection()->getDatabasePlatform()->registerDoctrineTypeMapping('tsrange', 'tsrange');

        return $em;
    }
}

