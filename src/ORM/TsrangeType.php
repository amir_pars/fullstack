<?php

namespace DevXYZ\Project\ORM;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class TsrangeType extends Type {

    const TSRANGE = "tsrange";

    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform) {
        return $platform->getDoctrineTypeMapping("tsrange");
    }

    public function convertToPHPValue($value, AbstractPlatform $platform) {
        $range = json_decode($value);
        return [new \DateTime($range[0]),new \DateTime($range[1])];
    }

    //(longitude, latitude)
    //this does not work because it includes a call to a function , f_hoo_hours
    //will use native sql instead to do inserts
    public function convertToDatabaseValue($value, AbstractPlatform $platform) {
        return "f_hoo_hours('" . $value[0]->format("Y-m-d H:iO"). "','" .$value[1]->format("Y-m-d H:iO") . "')";
    }

    public function getName() {
        return self::TSRANGE;
    }
}
