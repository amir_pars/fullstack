<?php

namespace DevXYZ\Project\Controller;

abstract class Base {

    protected $payload;
    protected $container;
    protected $em; //doctrine entity manager

    function __construct() {
        $request_body = file_get_contents('php://input');
        $this->payload = json_decode($request_body, true);
        $this->container = new \League\Container\Container;
        $this->container->add('database', 'DevXYZ\Project\Service\Database');
        $this->em = $this->container->get('database')->getEntityManager();
    }
}