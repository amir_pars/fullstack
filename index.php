<?php

require 'vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use League\Route\Strategy\RestfulStrategy;
use League\Route\RouteCollection;

$request = Request::createFromGlobals();
$router = new RouteCollection();

$router->setStrategy(new RestfulStrategy());

$router->addRoute('POST', '/meme', 'DevXYZ\Project\Controller\Meme::create');
$router->addRoute('GET', '/meme/{id:\d+}', 'DevXYZ\Project\Controller\Meme::retrieve');
$router->addRoute('POST', '/memes', 'DevXYZ\Project\Controller\Meme::retrieve_all');
$router->addRoute('PUT', '/meme', 'DevXYZ\Project\Controller\Meme::update');
$router->addRoute('DELETE', '/meme/{id:\d+}', 'DevXYZ\Project\Controller\Meme::delete');
$router->addRoute('GET', '/meme/query/{query}', 'DevXYZ\Project\Controller\Meme::search');

$dispatcher = $router->getDispatcher();
$response = $dispatcher->dispatch($request->getMethod(), $request->getPathInfo());
$response->send();