<?php

    namespace DevXYZ\Project\Entity;

    /**
     * DRY super class contains common fields id, created_at and date_udpated.
     * The created_at and updated_at fields are automatically populated
     * using PrePersist and PreUpdate LifeCycle calls (see updateTimestamps)
     */

    /**
     * @MappedSuperclass
     * @HasLifecycleCallbacks
     */
    abstract class Base
    {
        /**
         * @Id
         * @Column(type="integer")
         * @GeneratedValue(strategy="IDENTITY")
         */
        public $id;

        /**
         * @Column(type="datetimetz")
         */
        public $created_at;

        /**
         * @Column(type="datetimetz")
         */
        public $updated_at;

        /**
         * @PrePersist
         * @PreUpdate
         */
        public function updateTimestamps()
        {
            $this->updated_at = new \DateTime(date('Y-m-d H:i:s'));

            if($this->created_at == null)
            {
                $this->created_at = $this->updated_at;
            }
        }
    }