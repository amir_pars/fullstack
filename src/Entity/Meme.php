<?php

    namespace DevXYZ\Project\Entity;
    use Doctrine\Common\Collections\ArrayCollection;

    /**
     * @Entity(repositoryClass="DevXYZ\Project\Repository\Meme")
     * @Table(name="memes")
     */
    class Meme extends Base {

        public function __construct() {
            $this->tags = new ArrayCollection();
        }

        /**
         * @Column(type="string", length=255, nullable=true)
         */
        public $title;

        /**
         * @ManyToMany(targetEntity="\DevXYZ\Project\Entity\Tag",indexBy="id")
         * @JoinTable(name="memes_tags_join",
         *      joinColumns={@JoinColumn(name="meme_id", referencedColumnName="id", onDelete="CASCADE")},
         *      inverseJoinColumns={@JoinColumn(name="tag_id", referencedColumnName="id")}
         *      )
         * */
        public $tags;
    }

