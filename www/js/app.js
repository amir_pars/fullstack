/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


angular.module('devxyz', ['ngMaterial'])
        .controller('MemeCtrl', function ($scope, $mdDialog, Data) {

            $scope.search = {
                'title': '',
                'tags': ''
            };

            $scope.memes = [];

            $scope.filter = function() {
                Data.retrieve_all($scope.search).then(function(rs) {
                    console.log(rs.data);
                    $scope.memes = rs.data;
                });
            };

            $scope.filter();

            $scope.add_meme = function(meme) {

                meme.image = document.getElementById('file');

                Data.create(meme).then(function(rs) {
                    var copy = angular.copy(meme,copy);
                    copy.id = rs.id;
                    $scope.memes.push(copy);
                    //append meme to $scope memes
                }, function(rs) {
                    //display error message
                    alert(rs.data);
                });
            };

            $scope.delete_meme = function (ev, id) {
                // Appending dialog to document.body to cover sidenav in docs app
                var confirm = $mdDialog.confirm()
                        .title('Would you like to delete this meme?')
                        .textContent('Delete Meme')
                        .ariaLabel('Lucky day')
                        .targetEvent(ev)
                        .ok('Please do it!')
                        .cancel('No Way!');
                $mdDialog.show(confirm).then(function () {
                    return Data.remove(id).then(function() {
                        //remove deleted meme from scope
                        for(var i = 0; i<$scope.memes.length; i++) {
                            if($scope.memes[i].id == id) {
                                $scope.memes.splice(i, 1);
                                break;
                            }
                        }
                    });
                }, function () {
                    $scope.status = 'Can';
                });
            };

            $scope.update_meme = function(meme) {
                return Data.update(meme);
            };
        })

        .factory('Data', function ($http) {
            var appData = {
                webserviceUrl: 'http://173.57.148.5:81/fullstack/',
                retrieve_all: function (search) {
                    return $http.post(appData.webserviceUrl + 'memes', search);
                },
                create: function(meme) {
                    /*
                    var fd = new FormData();
                    fd.append('file', meme.file);
                    fd.append('meme')
                    */
                    return $http.post(appData.webserviceUrl + 'meme', meme);
                },
                remove: function(id) {
                    return $http.delete(appData.webserviceUrl + 'meme/' + id);
                },
                update: function(meme) {
                    return $http.put(appData.webserviceUrl + 'meme/' + meme.id, meme);
                }
            };

            return appData;
        })

        .directive('fileModel', ['$parse', function ($parse) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    var model = $parse(attrs.fileModel);
                    var modelSetter = model.assign;

                    element.bind('change', function(){
                        scope.$apply(function(){
                            modelSetter(scope, element[0].files[0]);
                        });
                    });
                }
            };
        }]);